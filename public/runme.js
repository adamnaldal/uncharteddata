var thomas
var me
var doll
var thomasMap
var meMap
var bodyMap
var slideTime
var slider
var timeSlack = 50;
var infoCards = [];

function preload(){
  thomas = loadJSON("res/data/Thomas.json")
  me = loadJSON("res/data/Me.json")
  doll = loadImage("res/images/doll.png")
  bodyMap = loadJSON("res/data/body_map.json")
}

function setup() {
  frameRate(60)
  createCanvas(windowWidth, windowHeight)
  colorMode(HSB, 360, 100, 100, 100)
  angleMode(DEGREES);
  strokeWeight(1);

  thomasMap = new StretchMap("thomas",thomas.stretches,0,0, 240)
  meMap = new StretchMap("me",me.stretches,700,0, 165)


  slider = createSlider(0, 2400, 1200)
  slider.position(450,740)
  slider.style('width', '550px');
}

function draw() {
  background(360)

  slideTime = slider.value();
  text(parseTime(slideTime), 447 + (map(slideTime, 0, 2400, 0, 530)), 730)

  // clears all previous texs and infoCards
  for (let each of infoCards){
    each.content.remove()
  }
  infoCards = [];

  thomasMap.visData()
  meMap.visData()


  // sets position of and displays all infocards
  for (let i = 0; i < infoCards.length; i++){
    if (i<2){
      infoCards[i].show(mouseX, mouseY + (infoCards[i].height * i))
    } else {
      infoCards[i].show(mouseX + (infoCards[i].width * floor(i/2)), mouseY + (infoCards[i].height * (i%2)))
    }
  }


}

function parseTime(timecode){
  let _timecode = str(timecode)
  if (_timecode < 18){
    return str("00:0" + floor(map(_timecode, 0, 99, 0, 60)))
  } else if (_timecode < 100){
    return str("00:" + floor(map(_timecode, 0, 99, 0, 60)))
  } else if (_timecode < 1000){
    let hour = _timecode.slice(0,1)
    let minute = floor(map(_timecode.slice(1,3), 0, 99, 0, 60))
    if (minute < 10){
      return str("0" + hour + ":0" + minute)
    } else {
      return str("0" + hour + ":" + minute)
    }

  } else {
    let hour = _timecode.slice(0,2)
    let minute = floor(map(_timecode.slice(2,4), 0, 99, 0, 60))
    if (minute < 10){
      return str(hour + ":0" + minute)
    } else {
      return str(hour + ":" + minute)
    }
  }
}

class StretchMap{
  constructor(name, data, pos_x, pos_y, hue){
    this.name = name;
    this.data = data;
    this.x = pos_x;
    this.y = pos_y;
    this.dots = [];
    this.visibleDots = [];
    this.hue = hue;
    this.ofsetMagnitude = 15;

    for (let each of this.data){
      if (bodyMap[each.bodypart]){
        //print(bodyMap[each.bodypart])

        // determines dot position relative to the figurine
        let circleX = bodyMap[each.bodypart].x + this.x
        let circleY = bodyMap[each.bodypart].y + this.y

        // determines dot size
        let dotSize;
        if (each.bodypart == "whole body"){
          dotSize = 340
        } else if (each.bodypart == "upper body"){
          dotSize = 120
        } else {
          dotSize = 20
        }

        // adjust dot position so they are spread out around a point
        let xOfset = this.ofsetMagnitude * sin( map(this.data.indexOf(each), 0, this.data.length, 0, 360) )
        let yOfset = this.ofsetMagnitude * cos( map(this.data.indexOf(each), 0, this.data.length, 0, 360) )
        circleX += xOfset
        circleY += yOfset

        this.dots.push(new InfoDot(circleX, circleY, dotSize, this.hue, each))
      }
    }
  }

  visData(){
    image(doll,this.x,this.y)

    // show elements inside timeframe
    this.visibleDots = []
    for (let i = 0; i < this.dots.length; i++){
      let each = this.dots[i];
      // if time of event is within timeframe move from hidden dots to visible dots
      if (each.timecode < (slideTime + timeSlack) && each.timecode > (slideTime - timeSlack)){
        this.visibleDots.push(each)
      }
    }


    for (let each of this.visibleDots){
      each.show()
    }

    // ads infocards for each of the visible dots
    for (let each of this.visibleDots){
      if(dist(mouseX, mouseY, each.x, each.y) < each.size){
          infoCards.push(new InfoCard(each.info, this.hue))
      }
    }

  }
}

class InfoDot{
  constructor(x, y, size, hue, info){
    this.x = x;
    this.y = y;
    this.size = size;
    this.hue = hue;
    this.trasparency;
    this.info = info;
    this.timecode;

    let timeArray = this.info.time.split(".")
    let hour = timeArray[0]
    let minuteDecimal = floor(map(timeArray[1], 0, 59, 0, 99))
    this.timecode = hour.concat(minuteDecimal)
  }

  show(){
    push()
    fill(this.hue, 80, 85, 10)
    stroke(this.hue, 80, 50, 30)
    circle(this.x, this.y, this.size, this.size)
    pop()
  }
}

class InfoCard{
  constructor(info, hue){
    this.info = info;
    this.x;
    this.y;
    this.width = 150;
    this.height = 180;
    this.hue = hue;
    this.textWidth = this.width - 10

    this.content = createDiv()
    this.content.style('font-size', '14px');
    this.content.style('width', '130px');

    let date = createP("Date: " + this.info.date)
    let time = createP("Time: " + this.info.time)
    let bodypart = createP("Bodypart: " + this.info.bodypart)
    let affect = createP("Affect: " + this.info.affect)
    let text = selectAll('p')
    for (let each of text){
      each.style('bottom-margin', "20px")
    }

    this.content.child(date)
    this.content.child(time)
    this.content.child(bodypart)
    this.content.child(affect)
  }

  show(_x, _y){
    this.x = _x;
    this.y = _y;

    push()
    fill(360)
    stroke(this.hue, 80, 75)
    strokeWeight(2)

    rect(this.x, this.y, this.width, this.height, 10)
    pop()

    this.content.position(this.x + 10, this.y)

  }
}
